# if running bash
if [ -n "$BASH_VERSION" ]; then
    # include .profile if it exists
    if [ -f "$HOME/.profile" ]; then
      . "$HOME/.profile"
    fi
    # include .bashrc if it exists
    if [ -f "$HOME/.bashrc" ]; then
      . "$HOME/.bashrc"
    fi
fi

# Compatibility check for terminals without display
# Used for cron reminders
# [[ $DISPLAY ]] && xhost +local:$LOGNAME

# [[ -s "$HOME/.rvm/scripts/rvm" ]] && source "$HOME/.rvm/scripts/rvm" # Load RVM into a shell session *as a function*

# Export GPG key
# export GPG_TTY=$(tty)
export EDITOR=vim
export VISUAL=$EDITOR

# source '/home/julien/.kube/completion.bash.inc'

# . "$HOME/.cargo/env"

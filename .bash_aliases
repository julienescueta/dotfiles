# Make some possibly destructive commands more interactive.
# alias rm='rm -i'
# alias mv='mv -i'
# alias cp='cp -i'

# enable color support of ls and also add handy aliases
if [ -x /usr/bin/dircolors ]; then
    test -r ~/.dircolors && eval "$(dircolors -b ~/.dircolors)" || eval "$(dircolors -b)"
    alias ls='ls --color=auto'
    #alias dir='dir --color=auto'
    #alias vdir='vdir --color=auto'

    alias grep='grep --color=auto'
    alias fgrep='fgrep --color=auto'
    alias egrep='egrep --color=auto'
fi

# some more ls aliases
alias ll='ls --group-directories-first \
	     --almost-all \
	     --human-readable \
	     --classify \
	     -l'
alias la='ls --almost-all \
	     --group-directories-first \
	     --classify'

alias dirs='dirs -v'

# dotfile/configurations managed in git
alias config='/usr/bin/git --git-dir=/home/julien/.cfg/ --work-tree=/home/julien'

# Add an "alert" alias for long running commands.  Use like so:
#   sleep 10; alert
alias alert='notify-send --urgency=low -i "$([ $? = 0 ] && echo terminal || echo error)" "$(history|tail -n1|sed -e '\''s/^\s*[0-9]\+\s*//;s/[;&|]\s*alert$//'\'')"'

# Show last 100 entries from history
alias his='history | tail -n 100; echo "Only last 100. For full, type: history"'

# Copy to clipboard
alias pbcopy='xclip -selection clipboard'

# No more - only less
alias more=less

# Screen shots
alias screenshot='flameshot gui -d 3000'

alias python=python3


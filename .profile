# ~/.profile: executed by the command interpreter for login shells.
# This file is not read by bash(1), if ~/.bash_profile or ~/.bash_login
# exists.
# see /usr/share/doc/bash/examples/startup-files for examples.
# the files are located in the bash-doc package.

# the default umask is set in /etc/profile; for setting the umask
# for ssh logins, install and configure the libpam-umask package.
#umask 022

# if running bash
if [ -n "$BASH_VERSION" ]; then
    # include .bashrc if it exists
    if [ -f "$HOME/.bashrc" ]; then
	. "$HOME/.bashrc"
    fi
fi

# set PATH so it includes user's private bin if it exists
if [ -d "$HOME/bin" ] ; then
    PATH="$HOME/bin:$PATH"
fi

# set PATH so it includes user's private bin if it exists
if [ -d "$HOME/.local/bin" ] ; then
    PATH="$HOME/.local/bin:$PATH"
fi

# Export dev keys
export GPG_TTY=$(tty)
export GITHUB_TOKEN=b185dbb1b4cf91c79c8a8bd8752c619784f3c676

# Go
export GOPATH="$HOME/go"
export PATH="$PATH:/usr/local/go/bin/"
export PATH="$PATH:$GOPATH/bin"
export PATH="$PATH:$HOME/bin"

# export PATH="${KREW_ROOT:-$HOME/.krew}/bin:$PATH"

# Thinkific
# export THINKIFICPATH="$HOME/projects/thinkific"
# export GOPATH="$GOPATH:$THINKIFICPATH/workspace/go"
# export AWS_PROFILE_NAME=think-dev
# export SECRET_IDS=dev/rails-env-1,dev/rails-env-2

# Node Version Manager
# export NVM_DIR="$HOME/.nvm"
# [ -s "$NVM_DIR/nvm.sh" ] && \. "$NVM_DIR/nvm.sh"  # This loads nvm

# Add RVM to PATH for scripting. Make sure this is the last PATH variable change.
# export PATH="$PATH:$HOME/.rvm/bin"

# [[ -s "$HOME/.rvm/scripts/rvm" ]] && source "$HOME/.rvm/scripts/rvm" # Load RVM into a shell session *as a function*

# . "$HOME/.cargo/env"
